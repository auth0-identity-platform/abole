# Abole #



### What is Abole? ###

**Abole** is a communications software development kit (SDK) for **Auth0** that contains documentation, sample code, 
extensions, integrations, python APIs, and Android / XR / web apps that use **Auth0** for authentication and authorization.